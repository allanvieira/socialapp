var APIGEARS = {
	getGearOnline : function(){
		Meteor.call('verifyGears',function(error,result){
			if(error){
				alert(error);
			}else{
				if(result){
					if(result.retry){					
						Session.set('liveGear',result._id);						
						setTimeout(function(){
							var activeGear = Session.get('liveGear');
							Meteor.call('checkGearPong', activeGear, function(error,result){
								if(error){
									alert(error);
								}else{
									if(result){
										return result;
									}else{
										return null;
									}
								}
							});
						},3000);							
					}else{
						return activeGear._id;
					}						
				}else{					
					return null;					
				}					
			}
		});	
	}
}