
Template.accountface.helpers({
	isEditingAccount: function(){
		return Session.get('isEditingAccount') === this._id;
	},
	isDetailAccount: function(){
		return Session.get('isDetailsAccount') === this._id;
	},
	isCheckingAccount: function(){
		return Session.get('isCheckingAccount') === this._id;
	},
	missingVerify: function(status){
		var checkingAccount = Session.get('isCheckingAccount');
		return (status === 'insert' || status === 'invalid') && (checkingAccount == null);
	},
	getStatusDesc: function(status){
		if(status === 'insert'){
			return "<span class='yellow-text text-darken-4'>Verificação de usuário e senha pendente!</span>"
		}else if (status === 'false'){
			return "<span class='red-text text-darken-4'>Usuário ou Senha inválidos!</span>"
		}else{
			return "<span class='green-text text-darken-4'>Verificada com sucesso!</span>"
		}
	}
});

callAccountAction = function (action,accountId,callback){
	Meteor.call('verifyGears', function (error,result){
		if(result){
			Meteor.call(action,accountId,result,function(error,result){
				callback(error,result);
			});
		}else{
			var $toastContent = $('<span>Você não possui Gear online! <a href="/gears" class="waves-effect waves-light btn red darken-2 ">Criar Gear</a></span>');
			Materialize.toast($toastContent, 5000);
			Session.set('isCheckingAccount',null);
		}
	});
}

Template.accountface.events({
	'click a.edit': function(e, tpl){
		e.preventDefault();
		Session.set('isEditingAccount', this._id);
	},
	'submit form.form-edit': function(e, tpl){
		e.preventDefault();

		var userName = tpl.$('input[name="userName"]').val();
		var pass = tpl.$('input[name="pass"]').val();
		var self = this;

		if(userName.length){
			Meteor.call("accountFaceUpdate",this._id, userName, pass, function(error){
				if(error){
					alert(error.reason);
					Session.set('isEditingAccount',self._id);
					Tracker.afterFlush(function(){
						tpl.$('input[name="userName"]').val(nameUser);
						tpl.$('input[name="pass"]').val(pass);
						tpl.$('input[name="userName"]').focus();
					});
				}
			});
			Session.set('isEditingAccount',null);
		}
	},
	'click button.cancel':function(e,tpl){
		e.preventDefault();
		Session.set('isEditingAccount',null);
	},
	'click a.details':function(e,tpl){
		e.preventDefault();
		Session.set('isDetailsAccount',Session.get('isDetailsAccount') === this._id ? null : this._id);
	},
	'click a.remove': function(e, tpl){
		e.preventDefault();
		Accountsface.remove(this._id);
	},
	'click a.verify': function(e, tpl){
		e.preventDefault();
		var accountId = this._id;
		Session.set('isCheckingAccount',accountId);

		var action = 'accountFaceCheck';

		var callbackResult = function(error,result){
			if(result){
				var $toastContent = $('<span>'+result.message+'</span>');
				Materialize.toast($toastContent, 5000);
				Session.set('isCheckingAccount',null);
			}
		}

		callAccountAction(action,accountId,callbackResult);

	}
});
