/*Template.ginstructions.onRendered(function () {
    var instance = this
    console.log('rendered start');
    var instructions = Ginstructions.find({ownerId: Meteor.userId()});
	intructions = instructions.collection._docs._map;
    Meteor.defer(function(){
   		console.log(intructions);
    });
    console.log('render end');
});*/

Template.ginstructions.helpers({
	ginstructions : function(){
		return Ginstructions.find({ownerId: Meteor.userId()});
	},
	sendInstructions : function(_id, url, instruction, checkinstruction ){
		var instructionObj = {};
		instructionObj._id = _id;
		instructionObj.url = url;
		instructionObj.instruction = instruction;
		if(checkinstruction){
			instructionObj.checkinstruction = checkinstruction
		}
		console.log(JSON.stringify(instructionObj));
	},
	executed : function(result){
		return result == null; 
	}
});

Template.ginstructions.events({
	'click a.create': function(e, tpl){
		e.preventDefault();
		Session.set('isCreatingAccount', true);
	},
	'click a.cancel': function(e, tpl){
		e.preventDefault();
		Session.set('isCreatingAccount',false);
	},
	'submit form.create':function(e, tpl){
		e.preventDefault();

		userName = tpl.$('input[name=username]').val();
		userPass = tpl.$('input[name=userpass]').val();
		
		Meteor.call('accountInsert',userName,userPass,function(error){
			if(error){
				alert(error.reason);
				Session.set('isCreatingAccount', true);
				Tracker.afterFlush(function(){
					tpl.$('input[name=username]').val(userName);
					tpl.$('input[name=userpass]').val(userPass);
				});
			}
		});
		

		Session.set('isCreatingAccount',false)
	}
});