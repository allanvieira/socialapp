
Template.gears.helpers({
	isCreatingGear: function(){
		return Session.get('isCreatingGear');
	},
	gears : function(){
		return Gears.find({ownerId: Meteor.userId()});
	},
	generateName : function(){
		return navigator.userAgent;
	},
	verifyActiveGear : function(status){
		if(status === 'On'){
			Session.set('activeGear','Verificando Gears');
			setTimeout(function(){
				Meteor.call('verifyGears',function(error,result){
					if(error){
						alert(error);
					}else{
						if(result){
							var activeGear = Gears.findOne(result);
							Session.set('activeGear',activeGear.name + " - " + activeGear._id + "\n" + activeGear.lastResponse + "\n" + activeGear.isProcess );
						}else{
							Session.set('activeGear','');
						}
					}
				});
			},1000);
		}
	},
	getActiveGear : function(){
		return Session.get('activeGear');
	}
});

Template.gears.events({
	'click a.create': function(e, tpl){
		e.preventDefault();
		Session.set('isCreatingGear', true);
	},
	'click a.cancel': function(e, tpl){
		e.preventDefault();
		Session.set('isCreatingGear',false);
	},
	'submit form.create':function(e, tpl){
		e.preventDefault();

		var gearName = tpl.$('input[name=name]').val();
		var gear = {
			name : gearName,
			ownerId: Meteor.userId()
		};

		Gears.insert(gear, function(error, _id){
			if(error){
				alert(error);
				Session.set('isCreatingGear',false);
				Tracker.afterFlush(function(){
					tpl.$('input[name=name]').val(gearName);
				});
			}else{
				Session.set('isCreatingGear',true);
				Tracker.afterFlush(function(){
					tpl.$('p.gear_id').text(_id);
				});
			}
		});

		Session.set('isCreatingGear',true);

	}
});
