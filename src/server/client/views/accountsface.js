	

Template.accountsface.helpers({
	isCreatingAccount: function(){
		return Session.get('isCreatingAccount');
	},
	accountsface : function(){
		return Accountsface.find({ownerId: Meteor.userId()});
	}
});

Template.accountsface.events({
	'click a.create': function(e, tpl){
		e.preventDefault();
		Session.set('isCreatingAccount', true);
	},
	'click button.cancel': function(e, tpl){
		e.preventDefault();
		Session.set('isCreatingAccount',false);
	},
	'submit form.create':function(e, tpl){
		e.preventDefault();

		userName = tpl.$('input#inputEmail').val();
		userPass = tpl.$('input#inputPass').val();
		
		Meteor.call('accountFaceInsert',userName,userPass,function(error){
			if(error){
				alert(error.reason);
				Session.set('isCreatingAccount', true);
				Tracker.afterFlush(function(){
					tpl.$('input[name=username]').val(userName);
					tpl.$('input[name=userpass]').val(userPass);
				});
			}
		});
		

		Session.set('isCreatingAccount',false)
	}
});