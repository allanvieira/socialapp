
Template.gearsregister.helpers({
	isCreatingGear: function(){
		return Session.get('isCreatingGear');
	},
	gears : function(){
		return Gears.find({ownerId: Meteor.userId()});
	},
	generateName : function(){
		return navigator.userAgent;
	}
});

Template.gearsregister.events({
	'click a.create': function(e, tpl){
		e.preventDefault();
		Session.set('isCreatingGear', true);
	},
	'click a.cancel': function(e, tpl){
		e.preventDefault();
		Session.set('isCreatingGear',false);
	},
	'submit form.create':function(e, tpl){
		e.preventDefault();
		
		var gearName = tpl.$('input[name=name]').val();
		
		Meteor.call('creatGear',gearName,function(error,response){
			if(error){
				alert(error);
				Session.set('isCreatingGear',false);
				Tracker.afterFlush(function(){
					tpl.$('input[name=name]').val(gearName);	
				});
			}else{
				Session.set('isCreatingGear',true);
				Tracker.afterFlush(function(){
					tpl.$('p.gear_id').text(response);	
				});
			}
		})

		Session.set('isCreatingGear',true);
		
	}
});