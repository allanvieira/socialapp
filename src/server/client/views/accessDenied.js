Template.accessDenied.helpers({
	isSigningUp: function(){
		return Session.get('isSigningUp');
	}
});

Template.accessDenied.events({
    'submit form.login': function(event){

        event.preventDefault();
        var emailVar = event.target.loginEmail.value;
        var passwordVar = event.target.loginPassword.value;
        Meteor.loginWithPassword(emailVar, passwordVar,function(error){
            if(error){
                alert(error);
            }else{
                Router.go('gears');
            }
        });
    },
    'submit form.register' : function(event){
    	event.preventDefault();
    	var emailVar = event.target.email.value;
    	var passwordVar = event.target.password.value;
    	Accounts.createUser({
            email: emailVar,
            password: passwordVar
        });
        Session.set('isSigningUp',false);
    },
    'click button.signingup': function(event){
    	event.preventDefault();
    	Session.set('isSigningUp',true);
    },
    'click button.cancel': function(event){
    	event.preventDefault();
    	Session.set('isSigningUp',false);
    }
})