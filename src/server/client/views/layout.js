
Template.layout.onRendered(function(){	
		this.$(".dropdown-button").dropdown({
    		hover:true
  		});  	
  		this.$(".button-collapse").sideNav();		        
});

Template.layout.events({
	'click a.logout': function(event){
		event.preventDefault();
		Meteor.logout();
	},	
	'click a.login': function(event){
		event.preventDefault();
		Router.go('accessDenied')
	}	
});

