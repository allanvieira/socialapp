Template.accountsmenu.helpers({
	getTemplate : function(){
		return Session.get('template');
	}
});

Template.accountsmenu.onRendered(function(){	
});

Template.accountsmenu.events({
	'click a.accountssocial': function(event,tpl){
		event.preventDefault();
		var template = event.currentTarget;
		Session.set('template',template.dataset.template);		
	}
});

