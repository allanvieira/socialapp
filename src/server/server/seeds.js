Meteor.startup(function(){
	var dummyUserEmail = "test@teste.com";

	if (Meteor.users.find({"emails.address": dummyUserEmail}).count() == 0) {

		var ownerId = Accounts.createUser({
			email: dummyUserEmail,
			password: '123'
		});

		scriptinstructions = [{
			app : 'facebook',
			action : 'validate',
			url : 'https://www.facebook.com',
			script : "document.getElementById('email').value = 'REPLACEUSUARIO';document.getElementById('pass').value = 'REPLACESENHA';document.getElementById('login_form').submit();",
			checkscript : "form=document.getElementById('login_form');if(!form){getElementById('ipcreturn').textContent='true'}else{getElementById('ipcreturn').textContent='false'}"
		},{
			app : 'facebook',
			action : 'logout',
			url : 'https://www.facebook.com/help/contact/logout?id=260749603972907',
			script : "document.querySelector('button[name=\"logout\"]').click();",
			checkscript : "form=document.querySelector('form[action=\"/ajax/help/contact/submit/page\"]');if(!form){document.getElementById('ipcreturn').textContent = 'false'}else{document.getElementById('ipcreturn').textContent='true'}"
		},{
			app : 'facebook',
			action : 'getUserId',
			url : 'https://www.facebook.com',
			script : "ipcreturn=document.getElementById('ipcreturn');var objaccount = {}; objaccount.id=document.querySelector('div#userNav li.sideNavItem a._5afe').href.split('?')[0].split('/')[3] === 'profile.php' ? document.querySelector('div#userNav li.sideNavItem a._5afe').href.split('?')[1].split('&')[0].split('=')[1] : document.querySelector('div#userNav li.sideNavItem a._5afe').href.split('?')[0].split('/')[3]; objaccount.desc = document.querySelector('div#userNav li.sideNavItem a._5afe').title; ipcreturn.textContent = JSON.stringify(objaccount);"
		},{
			app : 'facebook',
			action : 'getImg',
			url : 'https://www.facebook.com/REPLACEUSERID',
			script : "document.querySelector('a.profilePicThumb').click();",
			checkscript : "document.getElementById('ipcreturn').textContent = document.querySelector('img.spotlight').src"
		}]

		for(i in scriptinstructions){
				Instructionsscript.insert(scriptinstructions[i]);
		}

	}
});
