Meteor.publish("gears", function(query, options){

	if (options) {
    	check(options, {
      		sort: Match.Optional(Object),
      		limit: Match.Optional(Number),
    		fields: Match.Optional(Object)
    	});
  	}

  	query.ownerId = this.userId;

	return Gears.find(query,options);
});

Meteor.publish("instructionsscript", function(){
	return Instructionsscript.find({});
});


Meteor.publish("accountsface", function(){
	return Accountsface.find({ownerId: this.userId});
});

Meteor.publish("ginstructions", function(query, options){
	
	if (options) {
    	check(options, {
      		sort: Match.Optional(Object),
      		limit: Match.Optional(Number),
    		fields: Match.Optional(Object)
    	});
  	}

	query.ownerId = this.userId;
	return Ginstructions.find(query,options);
});