Accountsface = new Mongo.Collection('accountsface');

Accountsface.allow({
	insert: function(userId, doc){
		return (userId && doc.ownerId === userId);
	},
	update: function(userId, doc, fields, modifier){
		return doc.ownerId === userId;
	},
	remove: function(userId, doc){
		return doc.ownerId === userId;
	},
	fetch: ['ownerId']
});

if(Meteor.isServer){
	sendInstruction = function(app,action,replaces,gearId,callback){
		var scriptinstruction = Instructionsscript.findOne({app:app,action:action});

		if(scriptinstruction){
			var instruction = {
				ownerId : Meteor.userId(),
				gearId : gearId,
				instruction : scriptinstruction.script,
				url : scriptinstruction.url,
				result : " "
			};

			if(scriptinstruction.checkscript){
				instruction.checkinstruction = scriptinstruction.checkscript;
			}

			for(r in replaces){
				instruction.url = instruction.url.replace(r,replaces[r]);
				instruction.instruction = instruction.instruction.replace(r,replaces[r]);
			}

			var instructionId = Ginstructions.insert(instruction,function(error){
				if(error){
					throw new Meteor.Error("Não foi possivel enviar instrucao","Não foi possivel enviar instrucao");
				}else{
					var newInstruction =  Ginstructions.findOne(instructionId);
					while (!newInstruction.result) {
						newInstruction =  Ginstructions.findOne(instructionId);
					}
					return callback && callback(null,newInstruction.result);
				}
			});
		}else{
			return callback && callback(null,null);
		}

	}

	getUserId = function(gearId,accountId,callback){
		var sendInstructionSync = Meteor.wrapAsync(sendInstruction);
		var userId = sendInstructionSync('facebook','getUserId',{},gearId);
		if(userId){
			userId = JSON.parse(userId);
			Accountsface.update({_id:accountId}, {$set:{
				userId : userId.id,
				userDesc : userId.desc
			}});
			return callback && callback(null,userId.id);
		}else{
			return callback && callback('Erro ao buscar id da conta do facebook',null);
		}
	}

	getImg = function(gearId,accountId,userId,callback){
		var sendInstructionSync = Meteor.wrapAsync(sendInstruction);
		var result = sendInstructionSync('facebook','getImg',{'REPLACEUSERID':userId},gearId);
		if(result){
			Accountsface.update({_id:accountId}, {$set:{
				img : result
			}});
			return callback && callback(null,result);
		}else{
			return callback && callback('Erro ao busca link da foto do perfil do facebook');
		}
	}

	getAccountDetails = function(gearId,accountId){
		getUserId(gearId,accountId,function(error,result){
			if(result){
				getImg(gearId,accountId,result,function(result,error){
					if(result){
						console.log(result);
					}
				});
			}
		});
	}

	Meteor.methods({
		accountFaceInsert : function(userName, pass){
			check(Meteor.userId(),String);
			check(userName, String);
			check(pass, String);

			var account = {
				userName : userName,
				pass : pass,
				ownerId : Meteor.userId(),
				status : 'insert',
				img: 'img/default-user.png'
			}

			Accountsface.insert(account);

		},
		accountFaceUpdate : function(accountId, userName, pass){
			check(Meteor.userId(),String);
			check(accountId, String);
			check(userName, String);
			check(pass, String);

			var account = Accountsface.findOne(accountId);
			if(account){
				Accountsface.update({_id:accountId},{$set :{userName:userName, pass:pass}});
				return accountId;
			}else{
				throw new Meteor.Error("Conta nao Encontrada","Esta conta nao foi encontrada");
			}


		},
		accountFaceCheck : function(accountId,gearId){
			check(Meteor.userId(),String);
			check(accountId, String);
			check(gearId, String);

			//tenta buscar a conta
			var account = Accountsface.findOne(accountId);
			if(account){

				var replaces = {};
				replaces['REPLACEUSUARIO'] = account.userName;
				replaces['REPLACESENHA'] = account.pass;

				var sendInstructionSync = Meteor.wrapAsync(sendInstruction);
				var resultLogout = sendInstructionSync('facebook','logout',[],gearId);
				var result = sendInstructionSync('facebook','validate',replaces,gearId);

				if(resultLogout){
					if(result){
						if(result == 'false'){
							Accountsface.update({_id:accountId}, {$set:{
									status : 'false'
							}});
							return {message:"<span>Senha ou usuário inválido!</span>"}
						}else{
							//var validate = JSON.parse(result);
							Accountsface.update({_id:accountId}, {$set:{
									status : 'verify'
							}});
							getAccountDetails(gearId,accountId);
							return {message:"<span>Conta verificada com sucesso!</span>"}
						}
					}else{
						return {message:"<span>Não foi possível verificar!</span>"}
					}
				}else{
					return {message:"<span>Não foi possível realizar o logout!</span>"}
				}

			}else{
				throw new Meteor.Error("Conta nao Encontrada","Esta conta nao foi encontrada");
			}

		}
	});

}
