Gears = new Mongo.Collection('gears');

Gears.allow({
	insert: function(userId, doc){
		return (userId && doc.ownerId === userId);
	},
	update: function(userId, doc, fields, modifier){
		return doc.ownerId === userId;
	},
	remove: function(userId, doc){
		return doc.ownerId === userId;
	},
	fetch: ['ownerId']
});

if(Meteor.isServer){

	deactivateGearsOn = function(){
		var gearsActive = Gears.find({status:'On'});

		gearsActive.fetch().forEach(function(gear){
			Gears.update(gear._id,{$set: {status:'Off'}});
		});
	}

	sendPingToGear = function(gearId){
		var instruction = {
			ownerId : Meteor.userId(),
			gearId : gearId,
			instruction : 'ping',
			url : 'www.ping.com',
			result : " "
		};

		Ginstructions.insert(instruction,function(error){
			if(error){
				throw new Meteor.Error("Não foi possivel enviar ping","Não foi possivel enviar ping");
				console.log(error);
			}else{
				return instruction._id;
			}
		});

	};

	checkGearOnline = function(callback){
		//captura gear ativa
		gearActive = Gears.find({status:'On'}).fetch()[0];
		if(gearActive){
			console.log('Gear Online Encontrada');
			/*se ja respondeu alguma vez*/
			if(gearActive.lastResponse){
				console.log('Ja respondeu');
				var now = new Date();
				var diff = now - gearActive.lastResponse;
				/*ultima resposta a mais de 7 segundos*/
				if(diff > 7000){
					console.log('Ultima resposta a mais de 7 segundos');
					if(gearActive.inProcess){
						/*inativa gears online se ela ainda estiver esperando retorno*/
						console.log('Gear ainda esta esperando retorno DESATIVAR');
						deactivateGearsOn();
						callback && callback(null,'Gear online nao responde!');
					}else{
						/*envia novo ping*/
						console.log('Enviando novo ping');
						sendPingToGear(gearActive._id);
						Gears.update(gearActive._id,{$set:{inProcess:true}},function(error,data){
							if(error){
								throw new Meteor.Error(error,error);
							}
						});
						var result = {};
						result.retry = true;
						result._id = gearActive._id;
						callback && callback(null,result);
					}
					/*gear esta ativa*/

				}else{
					console.log('Gear respondeu em menos de 7 segundos atras GEAR ATIVA');
					callback && callback(null,gearActive);
				}
				/*se nao respondeu*/
			}else{
				console.log('Gear nunca respondeu');
				/*esta em processa de resposta*/
				if(gearActive.inProcess){
					console.log('Gear esta em processo de resposta');
					var now = new Date();
					var diff = now - gearActive.updateAt;
					if(diff > 7000){
						console.log('A mais de 7 segundos DESATIVAR GEAR');
						deactivateGearsOn();
						console.log('A menos de 7 segundos GEAR ATIVA');
						callback && callback(null,'Gear online nao responde!');
					}else{
						callback && callback(null,gearActive);
					}
					/*envia o primeiro ping da gear*/
				}else{
					console.log('Enviado primeiro ping');
					sendPingToGear(gearActive._id);
					Gears.update(gearActive._id,{$set:{inProcess:true}},function(error,data){
						if(error){
							throw new Meteor.Error(error,error);
						}
					});
					var result = {};
					result.retry = true;
					result._id = gearActive._id;
					callback && callback(null,result);
				}
			}
		}else{
			callback && callback(null,'Gear online não encontrada!');

		}
	};

	checkGearPong = function(gearId,callback){
		Meteor.setTimeout(function(){
			var activeGear = Gears.findOne(gearId);
			var now = new Date();
			if(now - activeGear.lastResponse > 4000){
				deactivateGearsOn();
				callback('gear nao responde a 4 segundos', null);
			}else{
				callback(null, activeGear._id);
			}
		},3000);
	}


Meteor.methods({
	gearUpdate: function(gearId,listening){
		check(Meteor.userId(),String);
		check(gearId, String);
		check(listening, Boolean);

		var gear = Gears.findOne(gearId);
		if(gear){

			Gears.update(gearId, {$set: {listening: listening}},function(error){
				if(!error){
				}
			});
			return gearId;
		} else{
			throw new Meteor.Error("gear-nao-existe","Esta gear nao existe");
		}
	},
	creatGear : function(name){
		var newgear = {
			name : name,
			ownerId: Meteor.userId(),
			status: 'On',
			inProcess: false
		};

		deactivateGearsOn();

		gearId = Gears.insert(newgear, function(error, _id){
			if(error){
				return error;
			}
		});
		return gearId;
	},
	verifyGears : function(){
		var checkGearOnlineSync = Meteor.wrapAsync(checkGearOnline);
		var checkGearPongSync = Meteor.wrapAsync(checkGearPong);
		var activeGear = checkGearOnlineSync();
		if(activeGear._id){
			if(activeGear.retry){
				var gearPong = checkGearPongSync(activeGear._id);
				return gearPong;
			}else{
				return activeGear._id;
			}
		}else{
			return null;
		}
	}
});
}
