Ginstructions = new Mongo.Collection('ginstructions');

Ginstructions.allow({
	insert: function(userId, doc){
		return (userId && doc.ownerId === userId);
	},
	update: function(userId, doc, fields, modifier){
		return doc.ownerId === userId;
	},
	remove: function(userId, doc){
		return doc.ownerId === userId;
	},
	fetch: ['ownerId']
});

Meteor.methods({
	instructionInsert : function(gearId,instruction,checkinstruction,url){
		check(Meteor.userId(),String);
		check(gearId, String);
		check(instruction, String);
		check(url, String);

		var gear = Gears.findOne(gearId);
		if(gear){		
			var instruction = {
				ownerId : Meteor.userId(),
				gearId : gearId,
				instruction : instruction,
				url:url,
				result : " "			
			}
			if(checkinstruction){
				instruction.checkinstruction = checkinstruction;	
			}
			Gears.update(gear,{$set:{inProcess:true}});
			Ginstructions.insert(instruction);
		}else{
			throw new Meteor.Error("Gear nao Encontrada","A gear desta instrussão não foi encontrada");
		}

		
		
	},
	instructionUpdate : function(instructionId, result){
		check(Meteor.userId(),String);
		check(result, String);

		var instruction = Ginstructions.findOne(instructionId);
		var gear = Gears.findOne(instruction.gearId);

		if(instruction){
			if(gear){
				Ginstructions.update({_id:instructionId},{$set :{result:result}});
				Gears.update(gear,{$set :{lastResponse:new Date, inProcess:false}},function(error){
					if(error){
						console.log(error);
					}
				});
				return instructionId;
			}else{
				throw new Meteor.Error("Gear nao Encontrada","A gear desta instrussão não foi encontrada");
			}
		}else{
			throw new Meteor.Error("Instrucao nao Encontrada","Esta instrucao nao foi encontrada");
		}
		

	}
});