Router.configure({
	layoutTemplate: 'layout',
	loadingTemplate: 'loading'
});

Router.route('/login',{
	name: 'accessDenied'
});


Router.route('/useraccount',{
});

Router.route('/accountsmenu',{
	waitOn: function(){
		return Meteor.subscribe("accountsface");
	}
});

Router.route('/accountsface',{
	waitOn: function(){
		return Meteor.subscribe("accountsface");
	}
});

Router.route('/gears',{
	waitOn: function(){
		return Meteor.subscribe("gears",{},{sort: {createdAt:-1}});
	}
});

Router.route('/gearsregister',{
	waitOn: function(){
		return Meteor.subscribe("gears",{});		
	}
});

Router.route('/ginstructions/:_id',function () {
  		this.render('ginstructions');
	},{
		waitOn: function(){
		return Meteor.subscribe("ginstructions", {"gearId": this.params._id});
	}
});

var requireLogin = function(){
	if(!Meteor.user()){
		if(Meteor.loggingIn()){
			this.render("loading");
		}else{
			this.render("accessDenied");	
		}
	}else{
		this.next();
	}
}

Router.onBeforeAction(requireLogin);