Instructionsscript.attachSchema(new SimpleSchema({
	app:{
		type: String
	},
	action:{
		type: String
	},
	url:{
		type: String
	},
	script:{
		type: String
	},
	checkscript : {
		type: String,
		optional: true
	},
	createdAt: {
		type: Date,
		autoValue: function (){
			if (this.isInsert){
				return new Date();
			}else if (this.isUpsert){
				return {$setOnInsert: new Date()};
			}else{
				this.unset();
			}
		}
	},
	updateAt: {
		type: Date,
		autoValue: function(){
			if(this.isUpdate){
				return new Date();
			}
		},
		denyInsert: true,
		optional: true
	}
}));