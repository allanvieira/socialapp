Ginstructions.attachSchema(new SimpleSchema({
	ownerId:{
		type: String
	},
	gearId:{
		type: String
	},
	url:{
		type: String
	},
	instruction:{
		type: String
	},
	checkinstruction:{
		type: String,
		optional: true
	},
	result:{
		type: String,
		optional: true
	},
	createdAt: {
		type: Date,
		autoValue: function (){
			if (this.isInsert){
				return new Date();
			}else if (this.isUpsert){
				return {$setOnInsert: new Date()};
			}else{
				this.unset();
			}
		}
	},
	updateAt: {
		type: Date,
		autoValue: function(){
			if(this.isUpdate){
				return new Date();
			}
		},
		denyInsert: true,
		optional: true
	}
}));