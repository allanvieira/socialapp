Accountsface.attachSchema(new SimpleSchema({
	userName:{
		type:String,
		index: true,
		unique: true
	},
	userId:{
		type: String,
		optional: true
	},
	userDesc:{
		type: String,
		optional: true
	},
	pass:{
		type:String
	},
	ownerId:{
		type: String
	},
	status:{
		type: String
	},
	img:{
		type: String
	},
	createdAt: {
		type: Date,
		autoValue: function (){
			if (this.isInsert){
				return new Date();
			}else if (this.isUpsert){
				return {$setOnInsert: new Date()};
			}else{
				this.unset();
			}
		}
	},
	verifiedAt:{
		type: Date,
		optional: true
	},
	updateAt: {
		type: Date,
		autoValue: function(){
			if(this.isUpdate){
				return new Date();
			}
		},
		denyInsert: true,
		optional: true
	}
}));
