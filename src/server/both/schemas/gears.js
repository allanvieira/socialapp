Gears.attachSchema(new SimpleSchema({
	name:{
		type:String
	},
	ownerId:{
		type: String
	},	
	status:{
		type: String
	},
	inProcess : {
		type: Boolean
	},
	listening :{
		type: Boolean,
		optional: true	
	},
	createdAt: {
		type: Date,
		autoValue: function (){
			if (this.isInsert){
				return new Date();
			}else if (this.isUpsert){
				return {$setOnInsert: new Date()};
			}else{
				this.unset();
			}
		}
	},
	updateAt: {
		type: Date,
		autoValue: function(){
			if(this.isUpdate){
				return new Date();
			}
		},
		denyInsert: true,
		optional: true
	},
	lastResponse: {
		type: Date,
		optional: true
	}
}));